//
//  EmailViewController.swift
//  illumine
//
//  Created by solulab on 4/10/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseAuthUI

@objc(EmailViewController)
class EmailViewController: FUIEmailEntryViewController, UITextFieldDelegate,FUIAuthDelegate
{
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //override action of default 'Next' button to use custom layout elements'
        authUI.delegate = self
        self.title = "Sign in"

        var image = #imageLiteral(resourceName: "back")
        image = image.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(onBack(_:)))

        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
    }
    func passwordSignUpViewController(forAuthUI authUI: FUIAuth, email: String) -> FUIPasswordSignUpViewController {
        return CreateAccountViewController.init(nibName: "CreateAccountViewController", bundle: Bundle.main, authUI: authUI, email: emailTextField.text!)
    }
    func passwordSignInViewController(forAuthUI authUI: FUIAuth, email: String) -> FUIPasswordSignInViewController {
        return PasswordViewController.init(nibName: "PasswordViewController", bundle: Bundle.main, authUI: authUI, email: emailTextField.text!)

    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        //update state of all UI elements (e g disable 'Next' buttons)
//        self.updateEmailValue(emailTextField)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.onBack()
    }
    @IBAction func onNextButton(_ sender: UIButton) {
        if let email = emailTextField.text {
            self.onNext(email)
        }
    }
    @IBAction func onCancel(_ sender: AnyObject) {
        self.cancelAuthorization()
    }
    
    @IBAction func onViewSelected(_ sender: AnyObject) {
        emailTextField.resignFirstResponder()
    }
    
    @IBAction func updateEmailValue(_ sender: UITextField) {
        if emailTextField == sender, let email = emailTextField.text {
            nextButton.isEnabled = !email.isEmpty
            self.didChangeEmail(email)
        }
    }
    
    // MARK: - UITextFieldDelegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField, let email = textField.text {
            self.onNext(email)
        }
        
        return false
    }
}
