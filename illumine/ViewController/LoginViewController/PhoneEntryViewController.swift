//
//  PhoneEntryViewController.swift
//  illumine
//
//  Created by solulab on 4/12/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseAuthUI
import MBProgressHUD
import FirebaseDatabase

class PhoneEntryViewController: FUIAuthBaseViewController,SWRevealViewControllerDelegate,countryPickerProtocol {

    @IBOutlet var txtPhone : customBottomLineTextField!
    @IBOutlet var pinView:SVPinView!
    @IBOutlet var viewPhoneNumber : UIView!
    @IBOutlet var viewCode : UIView!
    @IBOutlet var lblFlag : UILabel!
    @IBOutlet var lblNumber : UILabel!

    var verificationID : String!
    let countries:Countries = {
        return Countries.init(countries: JSONReader.countries())
    }()
    @IBOutlet weak var countryCodeTextField: customBottomLineTextField!
    var localeCountry:Country?
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = "Enter your phone number"
        var image = #imageLiteral(resourceName: "back")
        image = image.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(onBack(_:)))
        
        viewPhoneNumber.isHidden = false
        viewCode.isHidden = true
        addLocaleCountryCode()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configurePinView()
    }
    
    //MARK: - Private Functions
    private func addLocaleCountryCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            localeCountry = countries.list.filter {($0.iso2Cc == countryCode)}.first
            let filter = countries.list.filter { (dict) -> Bool in
                if dict.iso2Cc! == "IN"
                {
                    localeCountry = dict
                    return true
                }
                return false
            }
            print(filter)
            countryCodeTextField.text = (localeCountry?.iso2Cc!)! + " " + "(+" + (localeCountry?.e164Cc!)! + ")"
            lblFlag.text = localeCountry?.flag!
        }
    }
    @IBAction func didTapShowCountryCode(_ sender: Any) {
        let listScene = CountryCodeListController()
        listScene.delegate = self
        listScene.countries = countries
        navigationController?.pushViewController(listScene, animated: true)
    }
    
    //MARK: - countryPickerProtocol functions
    func didPickCountry(model: Country) {
        localeCountry = model
        countryCodeTextField.text = model.iso2Cc! + " " + "(+" + model.e164Cc! + ")"
        lblFlag.text = model.flag!
    }
    @IBAction func onBack(_ sender: AnyObject) {
        self.onBack()
    }
    @IBAction func btnVerifyPress(sender:UIButton)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let phone = "+\(localeCountry!.e164Cc!)\(txtPhone.text!)"
        lblNumber.text = phone
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error
            {
                UtilityClass.showAlert("\(error.localizedDescription)")
            }
            else
            {
                print("Verification code : \(verificationID!)")
                self.verificationID = verificationID!
                self.viewPhoneNumber.isHidden = true
                self.viewCode.isHidden = false
         
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
        }
    }
    @IBAction func btnContinuePress(sender:UIButton)
    {
        self.verifyCode(verificationID: verificationID!)
        
    }
    func verifyCode(verificationID : String) {
        let pin = self.pinView.getPin()
        guard !pin.isEmpty else {
            self.showAlert(title: "Error", message: "Pin entry incomplete")
            return
        }
//        self.showAlert(title: "Success", message: "The Pin entered is \(pin)")
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID,verificationCode: pin)
        MBProgressHUD.showAdded(to: self.view, animated: true)

        Auth.auth().signIn(with: credential) { (user, error) in
            MBProgressHUD.hide(for: self.view, animated: true)

            if error != nil {
                UtilityClass.showAlert("\(String(describing: error!.localizedDescription))")
            }
            else
            {
                user!.getIDToken(completion: { (token, error) in
                    if error == nil
                    {
                        let ref = Database.database().reference(fromURL: "https://myapplication-72fc7.firebaseio.com/")
                        
                        //Add token to instances table
                        ref.child("instances").child("\(user!.uid)/instanceId").setValue("\(token!)")
                        
                        var child : String!
                        if user!.phoneNumber != nil
                        {
                            child = "\(String(describing: user!.phoneNumber!))"
                        }
                        
                        // add uid in users table
                        ref.child("users").child("\(child!)/uid").setValue("\(user!.uid)")
                        
                        let tabbarObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
                        appDel.sideMenuviewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
                        
                        appDel.SWSideMenureval = SWRevealViewController(rearViewController: appDel.sideMenuviewObj, frontViewController: tabbarObj)
                        
                        appDel.SWSideMenureval?.delegate = self
                        appDel.window?.rootViewController = appDel.SWSideMenureval
                    }
                })
            }
        }
    }
    
    func configurePinView() {
        
        pinView.pinLength = 6
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 5
        pinView.textColor = UIColor.black
        pinView.underlineColor = UIColor.black
        pinView.underLineThickness = 2
        pinView.shouldSecureText = false
        
        pinView.font = UIFont.init(name: "OpenSans-Bold", size: 17)!
        pinView.keyboardType = .phonePad
        pinView.pinIinputAccessoryView = UIView()
        
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
    }
    func didFinishEnteringPin(pin:String) {
//        showAlert(title: "Success", message: "The Pin entered is \(pin)")
        print("The Pin entered is \(pin)")
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
