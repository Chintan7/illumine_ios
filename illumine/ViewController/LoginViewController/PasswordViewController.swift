//
//  PasswordViewController.swift
//  illumine
//
//  Created by solulab on 4/11/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseAuthUI
import MBProgressHUD
import FirebaseDatabase

class PasswordViewController: FUIPasswordSignInViewController,FUIAuthDelegate,SWRevealViewControllerDelegate {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var lblEmail : UILabel!
    var emailID : String!
    @IBOutlet var btnShow : UIButton!
    var isHide : Bool! = true
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, authUI: FUIAuth, email: String?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil, authUI: authUI, email: email)
        
        lblEmail.text = "You've already used \(email!) to sign in. Enter your password for that account."
        self.emailID = email
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authUI.delegate = self
        self.title = "Sign in"

        //override action of default 'Next' button to use custom layout elements'
        var image = #imageLiteral(resourceName: "back")
        image = image.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(onBack(_:)))
        
        if let button = self.navigationItem.rightBarButtonItem
        {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
        
//        let attrString1 = NSAttributedString(string: "You've already used ", attributes: [NSAttributedStringKey.font : UIFont(name: "OpenSans-Regular", size: 15)!,NSAttributedStringKey.foregroundColor : UIColor.black])
//
//        let attrString2 = NSAttributedString(string: "\(emailID)", attributes: [NSAttributedStringKey.font : UIFont.init(name: "OpenSans-Semibold", size: 15)!,NSAttributedStringKey.foregroundColor : UIColor.black])
//
//        let attrString3 = NSAttributedString(string: " to sign in. Enter your password for that account.", attributes: [NSAttributedStringKey.font : UIFont.init(name: "OpenSans-Regular", size: 15)!,NSAttributedStringKey.foregroundColor : UIColor.black])
//
//        let finalString = NSMutableAttributedString()
//        finalString.append(attrString1)
//        finalString.append(attrString2)
//        finalString.append(attrString3)
        
        lblEmail.text = emailID
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //update state of all UI elements (e g disable 'Next' buttons)
//        self.updateTextFieldValue(nil)
    }
   
    func passwordRecoveryViewController(forAuthUI authUI: FUIAuth, email: String) -> FUIPasswordRecoveryViewController {
        return PasswordRecoveryViewController.init(nibName: "PasswordRecoveryViewController", bundle: Bundle.main, authUI: authUI, email: self.emailID)
    }
    
    @IBAction func btnHideShowPasswordPress(sender:UIButton)
    {
        if isHide == true
        {
            passwordTextField.isSecureTextEntry = false
            isHide = false
            btnShow.setImage(#imageLiteral(resourceName: "show-icon"), for: .normal)
        }
        else
        {
            passwordTextField.isSecureTextEntry = true
            isHide = true
            btnShow.setImage(#imageLiteral(resourceName: "hide-icon"), for: .normal)
        }
    }
    @IBAction func onForgotPassword(_ sender: UIButton)
    {
        if let email = emailID
        {
            self.forgotPassword(forEmail: email)
        }
    }
    
    @IBAction func onNext(_ sender: UIButton?) {
        if let email = emailID, let password = passwordTextField.text
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in

                if error == nil
                {
                    print("\(String(describing: user!.uid))")
                    var email = user!.email!.replacingOccurrences(of: "@", with: "%40")
                    email = email.replacingOccurrences(of: ".", with: "%2")

                    let currentUser = Database.database().reference().child("users")
                    currentUser.child(email).observeSingleEvent(of: .value, with: { (snapshot) in
                            if ( snapshot.value is NSNull ) {
                                print("not found")
                            } else {
                                print(snapshot.value!)
                                UserDefaults.standard.set(snapshot.value!, forKey: "userProfile")
                                UserDefaults.standard.synchronize()

                                let tabbarObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
                                appDel.sideMenuviewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
                                
                                appDel.SWSideMenureval = SWRevealViewController(rearViewController: appDel.sideMenuviewObj, frontViewController: tabbarObj)
                                
                                appDel.SWSideMenureval?.delegate = self
                                appDel.window?.rootViewController = appDel.SWSideMenureval
                            }
                        })
                }
                else
                {
                    UtilityClass.showAlert("\(String(describing: error!.localizedDescription))")
                }
            })
        }
    }
    
    @IBAction func onCancel(_ sender: AnyObject) {
        self.cancelAuthorization()
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.onBack()
    }
    
    @IBAction func onViewSelected(_ sender: AnyObject) {
        passwordTextField.resignFirstResponder()
    }
    
    @IBAction func updateTextFieldValue(_ sender: AnyObject?) {
        if let email = emailID, let password = passwordTextField.text {
            nextButton.isEnabled = !email.isEmpty && !password.isEmpty
            self.didChangeEmail(email, andPassword: password)
        }
    }
    
    // MARK: - UITextFieldDelegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            self.onNext(nil)
        }
        
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
