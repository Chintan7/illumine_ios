//
//  TeacherOrParentViewController.swift
//  illumine
//
//  Created by solulab on 3/29/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

class TeacherOrParentViewController: UIViewController,SWRevealViewControllerDelegate
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        let button: UIButton = UIButton.init(type:UIButtonType.custom)
//        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
//        button.addTarget(self, action: #selector(self.btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
//        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
//
//        let barButton = UIBarButtonItem(customView: button)
//        self.navigationItem.leftBarButtonItem = barButton
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnParentPress(sender:UIButton)
    {
//        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "EmailExistViewController") as! EmailExistViewController
//        self.navigationController?.pushViewController(viewObj, animated: true)
//        
//        let tabbarObj = self.storyboard?.instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
//        appDel.sideMenuviewObj = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
//        
//        appDel.SWSideMenureval = SWRevealViewController(rearViewController: appDel.sideMenuviewObj, frontViewController: tabbarObj)
//        
//        appDel.SWSideMenureval?.delegate = self
//        appDel.window?.rootViewController = appDel.SWSideMenureval
    }
    @IBAction func btnLogoutPress(sender:UIButton)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
