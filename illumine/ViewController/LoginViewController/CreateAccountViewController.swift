//
//  CreateAccountViewController.swift
//  illumine
//
//  Created by solulab on 4/11/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseAuthUI
import FirebaseDatabase
import MBProgressHUD

@objc(CreateAccountViewController)
class CreateAccountViewController: FUIPasswordSignUpViewController, UITextFieldDelegate,SWRevealViewControllerDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet var btnShow : UIButton!
    var isHide : Bool! = true
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, authUI: FUIAuth, email: String?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil, authUI: authUI, email: email)
        
        emailTextField.text = email
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sign up"

        var image = #imageLiteral(resourceName: "back")
        image = image.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(onBack(_:)))
        
        if let button = self.navigationItem.rightBarButtonItem
        {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //update state of all UI elements (e g disable 'Next' buttons)
//        self.updateTextFieldValue(nil)
    }
    @IBAction func btnHideShowPasswordPress(sender:UIButton)
    {
        if isHide == true
        {
            passwordTextField.isSecureTextEntry = false
            isHide = false
            btnShow.setImage(#imageLiteral(resourceName: "show-icon"), for: .normal)
        }
        else
        {
            passwordTextField.isSecureTextEntry = true
            isHide = true
            btnShow.setImage(#imageLiteral(resourceName: "hide-icon"), for: .normal)
        }
    }
    
//    String uid = FirebaseAuth.getInstance().getUid();
//    String token = FirebaseInstanceId.getInstance().getToken();
//    if (null != uid && null != token)
//    {
//        FirebaseReference.instanceReference.child(uid).child("instanceId").setValue(token);
//        FirebaseReference.userReference.child(getUserId() + "/uid").setValue(uid);
//    }

    @IBAction func onNext(_ sender: UIButton?) {
        if let email = emailTextField.text,
            let password = passwordTextField.text,
            let username = usernameTextField.text {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if error == nil
                {
                    user?.getIDToken(completion: { (token, error) in
                        if error == nil
                        {
                            let ref = Database.database().reference(fromURL: "https://myapplication-72fc7.firebaseio.com/")
                            let instanceRef = ref
                                .child("instances")
                                .child("\(user!.uid)/instanceId").setValue("\(token!)")
                            
//                            let instanceID = instanceRef.key
//                            print("\(instanceID)")
//                            let instanceData = [
//                                "instanceId": token
//                            ]
//                            instanceRef.setValue(instanceData)
                            
                            var child : String!
                            if user?.phoneNumber != nil
                            {
                                child = "\(String(describing: user!.phoneNumber!))"
                            }
                            else
                            {
                                child = "\(String(describing: user!.email!))"
                                child = child.replacingOccurrences(of: "@", with: "%40")
                                child = child.replacingOccurrences(of: ".", with: "%2")
                            }
                            
                            let id = "\(ref.childByAutoId())"
                            let file_name = NSURL(fileURLWithPath: id).lastPathComponent!
                            ref.child("users").child("\(child!)/id").setValue("\(file_name)")
                            ref.child("users").child("\(child!)/studentId").setValue("\(file_name)")
                            
                            ref.child("users").child("\(child!)/uid").setValue("\(user!.uid)")
                            ref.child("users").child("\(child!)/name").setValue("\(username)")
                            ref.child("users").child("\(child!)/userType").setValue("STUDENT")

                            let tabbarObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
                            appDel.sideMenuviewObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
                            
                            appDel.SWSideMenureval = SWRevealViewController(rearViewController: appDel.sideMenuviewObj, frontViewController: tabbarObj)
                            
                            appDel.SWSideMenureval?.delegate = self
                            appDel.window?.rootViewController = appDel.SWSideMenureval
                        }
                    })

                }
                else
                {
                    UtilityClass.showAlert("\(String(describing: error!.localizedDescription))")
                }
//                if error != nil {
//                    print("DEVELOPER: Unable to authenticate with Firebase using email")
//                }else {
//                    print("DEVELOPER: Successfully authenticated with Firebase using email")
//                    if let user = user {
//                        let userData = ["provider": user.providerID, "userName": "\(user.displayName)"]
////                        self.completeMySignIn(user.uid, userData: userData)
//                    }
//                }
            })
//            self.signUp(withEmail: email, andPassword: password, andUsername: username)
        }
    }
    
    @IBAction func onCancel(_ sender: AnyObject) {
        self.cancelAuthorization()
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.onBack()
    }
    @IBAction func onViewSelected(_ sender: AnyObject) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        usernameTextField.resignFirstResponder()
    }
    
    // MARK: - UITextFieldDelegate methods
    
    @IBAction func updateTextFieldValue(_ sender: AnyObject?) {
        if let email = emailTextField.text,
            let password = passwordTextField.text,
            let username = usernameTextField.text {
            
            nextButton.isEnabled = !email.isEmpty && !password.isEmpty && !username.isEmpty
            self.didChangeEmail(email, orPassword: password, orUserName: username)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            usernameTextField.becomeFirstResponder()
        } else if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            self.onNext(nil)
        }
        
        return false
    }

}
