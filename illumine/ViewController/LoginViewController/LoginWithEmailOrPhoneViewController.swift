//
//  LoginWithEmailOrPhoneViewController.swift
//  illumine
//
//  Created by solulab on 3/28/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuthUI
import FirebasePhoneAuthUI

class LoginWithEmailOrPhoneViewController: UIViewController,FUIAuthDelegate,SWRevealViewControllerDelegate
{
    @IBOutlet var btnPhone : UIButton!
    @IBOutlet var btnEmail : UIButton!
    
    var authUI : FUIAuth!
    var auth:Auth?
    var authStateListenerHandle: AuthStateDidChangeListenerHandle?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Illumine"
        
        Utility.shared.dropShadow(view: btnPhone, color: blackColor, opacity: 0.5, radius: 3)
        Utility.shared.dropShadow(view: btnEmail, color: blackColor, opacity: 0.5, radius: 3)
        
        authUI = FUIAuth.defaultAuthUI()
//
//        // You need to adopt a FUIAuthDelegate protocol to receive callback
        authUI?.delegate = self
        let providers: [FUIAuthProvider] = [
            FUIPhoneAuth(authUI:self.authUI!)
            ]
        authUI?.providers = providers
//
//        self.loginAction(sender: self)
        return
    }
    @IBAction func loginAction(sender: AnyObject) {
        // Present the default login view controller provided by authUI
//        let authViewController = authUI?.authViewController();
//        appDel.window?.rootViewController = authViewController
//        provider.signIn(withPresenting: self);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        authUI?.delegate = self
//        if let button = self.navigationItem.leftBarButtonItem {
//            button.isEnabled = false
//            button.tintColor = UIColor.clear
//        }
    }
//    func authPickerViewController(forAuthUI authUI: FUIAuth) -> FUIAuthPickerViewController
//    {
//        return PhoneViewController(nibName: "PhoneViewController",bundle: Bundle.main,authUI: authUI)
//    }
//
//    func emailEntryViewController(forAuthUI authUI: FUIAuth) -> FUIEmailEntryViewController
//    {
//        return EmailViewController(nibName: "EmailViewController",bundle: Bundle.main,authUI: authUI)
//    }
    // Implement the required protocol method for FIRAuthUIDelegate
    
//    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
////        print(error?.localizedDescription)
//    }
//    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
////        print(user)
////        print(error?.localizedDescription)
//        if error == nil
//        {
//            let tabbarObj = self.storyboard?.instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
//            appDel.sideMenuviewObj = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
//
//            appDel.SWSideMenureval = SWRevealViewController(rearViewController: appDel.sideMenuviewObj, frontViewController: tabbarObj)
//
//            appDel.SWSideMenureval?.delegate = self
//            appDel.window?.rootViewController = appDel.SWSideMenureval
//        }
//    }
    
    @IBAction func btnEmailPress(sender:UIButton)
    {
        let viewObj = EmailViewController(nibName: "EmailViewController",bundle: Bundle.main,authUI: authUI)
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    @IBAction func btnPhonePress(sender:UIButton)
    {
        let viewObj = PhoneEntryViewController(nibName: "PhoneEntryViewController",bundle: Bundle.main,authUI: authUI)
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

