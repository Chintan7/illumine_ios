//
//  PasswordRecoveryViewController.swift
//  illumine
//
//  Created by solulab on 4/11/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseAuthUI
import MBProgressHUD

class PasswordRecoveryViewController: FUIPasswordRecoveryViewController, UITextFieldDelegate {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var recoverButton: UIButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, authUI: FUIAuth, email: String?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil, authUI: authUI, email: email)
        
        emailTextField.text = email
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Recover password"

        var image = #imageLiteral(resourceName: "back")
        image = image.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(onBack(_:)))
        
        if let button = self.navigationItem.rightBarButtonItem
        {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //update state of all UI elements (e g disable 'Next' buttons)
//        self.updateEmailValue(emailTextField)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.onBack()
    }
    
    @IBAction func onRecover(_ sender: UIButton) {
        if let email = emailTextField.text {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if error == nil
                {
                    self.onBack()
                    UtilityClass.showAlert("Follow the instructions sent to \(email) to recover your password.")
                }
                else
                {
                    UtilityClass.showAlert("\(error!.localizedDescription)")
                }
            })
//            self.recoverEmail(email)
        }
    }
    @IBAction func onCancel(_ sender: AnyObject) {
        self.cancelAuthorization()
    }
    
    @IBAction func updateEmailValue(_ sender: UITextField) {
        if emailTextField == sender, let email = emailTextField.text {
            recoverButton.isEnabled = !email.isEmpty
            self.didChangeEmail(email)
        }
    }
    
    @IBAction func onViewSelected(_ sender: AnyObject) {
        emailTextField.resignFirstResponder()
    }
    
    // MARK: - UITextFieldDelegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField, let email = textField.text {
            self.recoverEmail(email)
        }
        
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
