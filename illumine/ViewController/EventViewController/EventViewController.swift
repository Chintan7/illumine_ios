//
//  EventViewController.swift
//  illumine
//
//  Created by solulab on 3/23/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseDatabase

class EventViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SlideMenuDelegate
{
    @IBOutlet var txtSearch : customTextField!
    @IBOutlet var tblObj : UITableView!
    var SWRevalViewC : SWRevealViewController!
    var arrEvents = [JsonDictionary]()
    var arrUpcoming = [JsonDictionary]()
    var arrKeyUpcomig = [String]()
    var arrKeyEvent = [String]()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Events"
        SWRevalViewC = self.revealViewController()
        SWRevalViewC.tapGestureRecognizer()
        tblObj.tableFooterView = UIView()
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.menuButtonPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        Utility.shared.dropShadow(view: txtSearch, color: blackColor, opacity: 0.3, radius: 3)
        
        tblObj.rowHeight = UITableViewAutomaticDimension
        tblObj.estimatedRowHeight = 44
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        getEventDetail()
    }
    
    func getEventDetail()
    {
        appDel.showHUD()
        let currentUser = Database.database().reference().child("events")
        currentUser.observeSingleEvent(of: .value, with: { (snapshot) in
            appDel.hideHUD()
            if ( snapshot.value is NSNull )
            {
                print("not found")
            } else
            {
                print(snapshot.value!)
                if let dictMain = snapshot.value as? NSDictionary
                {
                    self.arrEvents = Array()
                    self.arrUpcoming = Array()
                    self.arrKeyEvent = Array()
                    self.arrKeyUpcomig = Array()

                    let arrKeys = dictMain.allKeys as! [String]
                    for key in arrKeys
                    {
                        let dict = dictMain[key] as! JsonDictionary
                        if let published = NSNumber.init(value: Float.init("\(dict["published"]!)")!) as? NSNumber
                        {
                            let val = Int(truncating: published)
                            if val == 1
                            {
                                let dictDate = dict["eventDate"] as! JsonDictionary
                                
                                let date = Date.init(timeIntervalSince1970: TimeInterval.init("\(dictDate["time"]!)")!/1000)
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateStyle = DateFormatter.Style.full
                                dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                                dateFormatter.timeZone = TimeZone.current
                                let localDate = dateFormatter.string(from: date)
                                print(localDate)
                                
                                let currentDate = Date()
                                dateFormatter.dateStyle = DateFormatter.Style.full
                                dateFormatter.timeStyle = DateFormatter.Style.medium
                                dateFormatter.timeZone = TimeZone.current
                                let localcurrentDate = dateFormatter.string(from: currentDate)
                                print(localcurrentDate)
                                
                                if date > currentDate
                                {
                                    print("date1 > date2")
                                    self.arrUpcoming.append(dict)
                                    self.arrKeyUpcomig.append(key)
                                }
                                else
                                {
                                    print("date1 < date2")
                                    self.arrEvents.append(dict)
                                    self.arrKeyEvent.append(key)
                                }
                            }
                        }
                    }
                    print(self.arrUpcoming)

                    print(self.arrEvents)
                    self.tblObj.reloadData()
                }
            }
        })
    }
   
    @IBAction func menuButtonPress(sender:UIButton)
    {
        self.view.endEditing(true)
        SWRevalViewC.revealToggle(sender)
        appDel.sideMenuviewObj.delegate = self
    }
    func pushViewController(viewcontroller: UIViewController)
    {
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    func menuButtonpress()
    {
        SWRevalViewC.revealToggle(self.navigationItem.leftBarButtonItem)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.section == 0
        {
            if let dict = arrUpcoming[indexPath.row]["eventImage"] as? String
            {
                return 250.0
            }
            else
            {
                return 120.0
            }
            
        }
        else
        {
            if let dict = arrEvents[indexPath.row]["eventImage"] as? String
            {
                return 250.0
            }
            else
            {
                return 120.0
            }
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if section == 0
        {
            if arrUpcoming.count == 0
            {
                return ""
            }
            return "Upcoming"
        }
        if section == 1
        {
            if arrEvents.count == 0
            {
                return ""
            }
            return "Previous"
        }
        return ""
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        var height : CGFloat = 0
        if section == 0
        {
            if arrUpcoming.count > 0
            {
                height = 40
            }
        }
        else
        {
            if arrEvents.count > 0
            {
                height = 40
            }
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: height))
        view.backgroundColor = UIColor.init(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: 100, height: height))
        if section == 0
        {
            label.text = "Upcoming"
        }
        else
        {
            label.text = "Previous"
            let imgLine = UIImageView.init(frame: CGRect.init(x: 110, y: 19, width: tableView.frame.size.width - 130, height: 1))
            imgLine.backgroundColor = UIColor.init(red: 213/255, green: 203/255, blue: 203/255, alpha: 1)
            view.addSubview(imgLine)
        }
        label.textColor = UIColor.black
        label.font = UIFont.init(name: "OpenSens-Semibold", size: 13)
       
        view.addSubview(label)
        
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            if arrUpcoming.count == 0
            {
                return 0.01
            }
            return 40
        }
        else
        {
            if arrEvents.count == 0
            {
                return 0.01
            }
            return 40
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return arrUpcoming.count
        }
        return arrEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as! eventListCell
        cell.selectionStyle = .none
        UtilityClass.setShedowIn(cell.bgView)
    
        if indexPath.section == 0
        {
            cell.lblEventName.text = "\(arrUpcoming[indexPath.row]["eventName"]!)"
            let attrString = NSMutableAttributedString(string: "\(arrUpcoming[indexPath.row]["description"]!)")
            let style = NSMutableParagraphStyle()
//            style.lineSpacing = 50 // change line spacing between paragraph like 36 or 48
//            style.minimumLineHeight = 20 // change line spacing between each line like 30 or 40
            attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: NSRange(location: 0, length: "\(arrUpcoming[indexPath.row]["description"]!)".count))
            attrString.addAttribute(NSAttributedStringKey.kern, value:10.0, range:NSRange(location: 0, length: "\(arrUpcoming[indexPath.row]["description"]!)".count))

            cell.lblEventDescription.attributedText = attrString
            let dictDate = arrUpcoming[indexPath.row]["eventDate"] as! JsonDictionary
            
            let date = Date.init(timeIntervalSince1970: TimeInterval.init("\(dictDate["time"]!)")!/1000)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "dd/MM/yyyy" //Specify your format that you want
            cell.lblEventDate.text = dateFormatter.string(from: date)
            
            cell.lblEventSubTitle.text = "\(arrUpcoming[indexPath.row]["creator"]!)"
            if let str = arrUpcoming[indexPath.row]["eventImage"] as? String
            {
                cell.imgView.sd_setImage(with: URL.init(string: str), placeholderImage: #imageLiteral(resourceName: "activity_placeholder"), options: [], completed: nil)
                cell.imgHeight.constant = 130
            }
            else
            {
                cell.imgHeight.constant = 0
            }
        }
        else
        {
            cell.lblEventName.text = "\(arrEvents[indexPath.row]["eventName"]!)"
            
            if let str = arrEvents[indexPath.row]["eventImage"] as? String
            {
                cell.imgView.sd_setImage(with: URL.init(string: str), placeholderImage: #imageLiteral(resourceName: "activity_placeholder"), options: [], completed: nil)
                cell.imgHeight.constant = 130
            }
            else
            {
                cell.imgHeight.constant = 0
            }
            
          //  cell.lblEventDescription.text = "\(arrEvents[indexPath.row]["description"]!)"
            let attrString = NSMutableAttributedString(string: "\(arrEvents[indexPath.row]["description"]!)")
            let style = NSMutableParagraphStyle()
            style.paragraphSpacingBefore = 10.0

//            style.lineSpacing = 24 // change line spacing between paragraph like 36 or 48
//            style.minimumLineHeight = 20 // change line spacing between each line like 30 or 40
            attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: NSRange(location: 0, length: "\(arrEvents[indexPath.row]["description"]!)".count))
//            attrString.addAttribute(NSAttributedStringKey.kern, value:10.0, range:NSRange(location: 0, length: "\(arrUpcoming[indexPath.row]["description"]!)".count))

            cell.lblEventDescription.attributedText = attrString
            let dictDate = arrEvents[indexPath.row]["eventDate"] as! JsonDictionary

            let date = Date.init(timeIntervalSince1970: TimeInterval.init("\(dictDate["time"]!)")!/1000)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "dd/MM/yyyy" //Specify your format that you want
            cell.lblEventDate.text = dateFormatter.string(from: date)
            
            cell.lblEventSubTitle.text = "\(arrEvents[indexPath.row]["creator"]!)"

        }
        
//        cell.pageControll.numberOfPages = 5

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailViewController") as! EventDetailViewController
        if indexPath.section == 0
        {
            viewObj.dictDetail = arrUpcoming[indexPath.row]
            viewObj.Eventkey = arrKeyUpcomig[indexPath.row]
        }
        else
        {
            viewObj.dictDetail = arrEvents[indexPath.row]
            viewObj.Eventkey = arrKeyEvent[indexPath.row]
        }
        self.pushViewController(viewcontroller: viewObj)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class eventListCell: UITableViewCell,UIScrollViewDelegate {
    @IBOutlet var bgView : UIView!
    @IBOutlet var lblEventName : UILabel!
    @IBOutlet var lblEventDate : UILabel!
    @IBOutlet var lblEventSubTitle : UILabel!
    @IBOutlet var lblEventDescription : UILabel!
//    @IBOutlet var pageControll : UIPageControl!
    @IBOutlet var imgHeight : NSLayoutConstraint!
//    @IBOutlet var scrBanner : UIScrollView!
    @IBOutlet var imgView : UIImageView!
    
//    override func awakeFromNib() {
//        // 4
//        let pagesScrollViewSize = CGSize(width: UIScreen.main.bounds.width - 56,height: scrBanner.frame.size.height)
//        scrBanner.contentSize = CGSize(width: pagesScrollViewSize.width * 5,height: pagesScrollViewSize.height)
////        scrBanner.contentSize = CGSize.init(width: (scrBanner.frame.width) * 5, height: 130)
//        var cnt : CGFloat = 0
//        for i in 0...1
//        {
//            cnt = CGFloat(i) * (UIScreen.main.bounds.width - 56)
//            imgView = UIImageView(frame: CGRect.init(x: cnt, y: 0, width: UIScreen.main.bounds.width - 56, height: scrBanner.contentSize.height))
//            imgView.tag = i
//            scrBanner.addSubview(imgView)
//        }
//    }
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
//    @IBAction func changePage(sender : UIPageControl)
//    {
//        let x = CGFloat(pageControll.currentPage) * scrBanner.frame.size.width
//        scrBanner.setContentOffset(CGPoint.init(x: x, y: 0), animated: true)
//    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
//    {
//        let pageNumber = round(scrBanner.contentOffset.x / scrBanner.frame.size.width)
//        pageControll.currentPage = Int(pageNumber)
//    }
}
