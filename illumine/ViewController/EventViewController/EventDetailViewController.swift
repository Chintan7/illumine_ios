//
//  EventDetailViewController.swift
//  illumine
//
//  Created by solulab on 4/2/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseDatabase

class EventDetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet var btnDetail : UIButton!
    @IBOutlet var btnMedia : UIButton!
    @IBOutlet var lblDetail : UILabel!
    @IBOutlet var lblMedia : UILabel!
    @IBOutlet var viewEventDetail : UIView!
    @IBOutlet var viewHeader : UIView!
    @IBOutlet var collectionEventMedia : UICollectionView!
    
    @IBOutlet var lblEventName : UILabel!
    @IBOutlet var lblEventDescription : UILabel!
    @IBOutlet var lblEventDate : UILabel!
    @IBOutlet var lblEventTime : UILabel!
    @IBOutlet var lblEventPlace : UILabel!
    var arrImageUrl = [String]()
    @IBOutlet var btnGoing : UIButton!
    var isGoing : Bool!
    var dictDetail = JsonDictionary()
    var Eventkey : String!
    var goingCount : Int! = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Test"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        btnDetailPress(UIButton())
        Utility.shared.dropShadow(view: viewEventDetail, color: blackColor, opacity: 0.3, radius: 3)
        Utility.shared.dropShadow(view: viewHeader, color: blackColor, opacity: 0.3, radius: 3)
        collectionEventMedia.clipsToBounds = false
        collectionEventMedia.layer.masksToBounds = false
        Utility.shared.dropShadow(view: collectionEventMedia, color: blackColor, opacity: 0.3, radius: 3)
        
        print(dictDetail)
        lblEventName.text = "\(dictDetail["eventName"]!)"
        lblEventDescription.text = "\(dictDetail["description"]!)"
        lblEventDescription.text = "\(dictDetail["description"]!)"

        let dictDate = dictDetail["eventDate"] as! JsonDictionary
        
        let date = Date.init(timeIntervalSince1970: TimeInterval.init("\(dictDate["time"]!)")!/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "EEEE,dd MMMM" //Specify your format that you want
        lblEventDate.text = dateFormatter.string(from: date)
        lblEventPlace.text = "Place - \(dictDetail["location"]!)"
        
        let date1 = Date(timeIntervalSince1970: TimeInterval.init("\(dictDate["time"]!)")!/1000)
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "hh:mm a"
        lblEventTime.text = "Time - \(dateFormatter1.string(from: date1))"
        
        if let dictGoing = dictDetail["goingPeopleMap"] as? NSDictionary
        {
            let arrKEys = dictGoing.allKeys as! [String]
            goingCount = arrKEys.count
            btnGoing.setTitle("\(goingCount!) going", for: .normal)

            let userDetail = UserDefaults.standard.value(forKey: "userProfile") as! [String:Any]
            let uID = "\(userDetail["id"]!)"
            
            if arrKEys.contains(uID)
            {
                btnGoing.setImage(#imageLiteral(resourceName: "checkbox_fill"), for: .normal)
                isGoing = true
            }
            else
            {
                btnGoing.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
                isGoing = false
            }
        }
        else
        {
            btnGoing.setTitle("0 going", for: .normal)
            btnGoing.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
            goingCount = 0
            isGoing = false
        }
        getMedia()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
    }
    func getMedia()
    {
        arrImageUrl = Array()
        appDel.showHUD()
        let ref = Database.database().reference().child("photos")
        ref.child(Eventkey).observeSingleEvent(of: .value, with: { (snapshot) in
            appDel.hideHUD()
            if ( snapshot.value is NSNull )
            {
                print("not found")
            } else {
                print(snapshot.value!)
                if let dictMain = snapshot.value as? NSDictionary
                {
                    let arrKeys = dictMain.allKeys as! [String]
                    for key in arrKeys
                    {
                        let dict = dictMain[key] as! JsonDictionary
                        self.arrImageUrl.append("\(dict["downloadUrl"]!)")
                    }
                }
                self.collectionEventMedia.reloadData()
            }
        })
    }
    @IBAction func btnCheckboxPress(sender:UIButton)
    {
        let userDetail = UserDefaults.standard.value(forKey: "userProfile") as! [String:Any]
        let uID = "\(userDetail["id"]!)"
        let usernameRef =  Database.database().reference().child("events").child(Eventkey).child("goingPeopleMap")
        if isGoing == true
        {
            goingCount = goingCount! - 1
            btnGoing.setTitle("\(goingCount!) going", for: .normal)
            usernameRef.child(uID).removeValue();
            btnGoing.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
            isGoing = false
        }
        else
        {
            usernameRef.child("/\(uID)").setValue("\(userDetail["name"]!)")
            goingCount = goingCount! + 1
            btnGoing.setTitle("\(goingCount!) going", for: .normal)
            btnGoing.setImage(#imageLiteral(resourceName: "checkbox_fill"), for: .normal)
            isGoing = true
        }
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDetailPress(_ radioButton: UIButton)
    {
        btnDetail.setImage(#imageLiteral(resourceName: "profile_selected"), for: .normal)
        btnMedia.setImage(#imageLiteral(resourceName: "media"), for: .normal)
        lblDetail.textColor = BlueColor
        lblMedia.textColor = UIColor.init(red: 161/255, green: 161/255, blue: 161/255, alpha: 1)
        viewEventDetail.isHidden = false
        collectionEventMedia.isHidden = true
    }
    
    @IBAction func btnMediaPress(_ radioButton: UIButton)
    {
        btnDetail.setImage(#imageLiteral(resourceName: "profile"), for: .normal)
        btnMedia.setImage(#imageLiteral(resourceName: "media_selected"), for: .normal)
        lblMedia.textColor = BlueColor
        lblDetail.textColor = UIColor.init(red: 161/255, green: 161/255, blue: 161/255, alpha: 1)
        viewEventDetail.isHidden = true
        collectionEventMedia.isHidden = false
    }
    
    func deselectMediaButton()
    {
        btnMedia.isSelected = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImageUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoCell
        cell.imgView.sd_setImage(with: URL.init(string: "\(arrImageUrl[indexPath.row])"), placeholderImage: #imageLiteral(resourceName: "activity_placeholder"), options: [], completed: nil)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "ViewPhotosViewController") as! ViewPhotosViewController
        viewObj.arrImgUrl =  arrImageUrl
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
class PhotoCell: UICollectionViewCell {
    @IBOutlet var imgView : UIImageView!
}
