//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import MBProgressHUD

protocol SlideMenuDelegate
{
    func menuButtonpress()
    func pushViewController(viewcontroller:UIViewController)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblUserNumber : UILabel!
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    var SWRevalViewC : SWRevealViewController!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        SWRevalViewC = self.revealViewController()
        SWRevalViewC.tapGestureRecognizer()
        self.tblMenuOptions.tableFooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.tblMenuOptions.frame.size.width, height: 1))
        
        if UIScreen.main.bounds.height <= 480
        {
            tblMenuOptions.isScrollEnabled = true
        }
        else
        {
            tblMenuOptions.isScrollEnabled = false
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        updateArrayMenuOptions()
        
     /*   MBProgressHUD.showAdded(to: self.view, animated: true)
        if Auth.auth().currentUser?.uid != nil
        {
            //            let user = Auth.auth().currentUser!
            let queryRef = Database.database().reference(fromURL: "https://myapplication-72fc7.firebaseio.com")
            
            queryRef.child(Auth.auth().currentUser!.uid).observe(.value, with: { (snapshot) -> Void in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if !snapshot.exists()
                {
                    return
                }
                else
                {
                    if let name = snapshot.childSnapshot(forPath: "name").key as? String
                    {
                        print(name)
                        self.lblUserName.text = name
                    }
                    else
                    {
                        self.lblUserName.text = ""
                    }
                    if let phoneNumber = snapshot.childSnapshot(forPath: "phoneNumber").key as? String
                    {
                        print(phoneNumber)
                        self.lblUserNumber.text = phoneNumber
                    }
                    else
                    {
                        self.lblUserName.text = ""
                    }
                }
            })
        }
        else
        {
            tblMenuOptions.delegate?.tableView!(tblMenuOptions, didSelectRowAt: IndexPath.init(row: 4, section: 0))
        }*/
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
       
    }
    
    func updateArrayMenuOptions()
    {
        arrayMenuOptions.append(["title":"Profile", "icon":"profile"])
        arrayMenuOptions.append(["title":"Holiday List", "icon":"holiday-list"])
        arrayMenuOptions.append(["title":"Media", "icon":"media"])
        arrayMenuOptions.append(["title":"Notifications", "icon":"notification_gray"])
        arrayMenuOptions.append(["title":"Sign out", "icon":"sign-out"])
      
        tblMenuOptions.reloadData()
    }   
    
    @IBAction func onCloseMenuClick(_ button:UIButton!)
    {
        btnMenu.tag = 0
        
        if (self.delegate != nil)
        {
            self.delegate?.menuButtonpress()
        }
       
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            self.btnCloseMenuOverlay.backgroundColor = UIColor.clear
            self.btnCloseMenuOverlay.alpha = 1
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.delegate?.menuButtonpress()
        if indexPath.row == 0
        {
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            viewObj.isFromProfile = true
            self.delegate?.pushViewController(viewcontroller: viewObj)
        }
        else if indexPath.row == 1
        {
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "HolidayListViewController") as! HolidayListViewController
            self.delegate?.pushViewController(viewcontroller: viewObj)
        }
        else if indexPath.row == 2
        {
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            viewObj.isFromProfile = false
            self.delegate?.pushViewController(viewcontroller: viewObj)
        }
        else if indexPath.row == 3
        {
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.delegate?.pushViewController(viewcontroller: viewObj)
        }
        else if indexPath.row == 4
        {
            try? Auth.auth().signOut()
            do {
                try Auth.auth().signOut()
                let loginObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginWithEmailOrPhoneViewController") as! LoginWithEmailOrPhoneViewController
                let navObj = UINavigationController.init(rootViewController: loginObj)
                appDel.window?.rootViewController = navObj
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1;
    }
}
