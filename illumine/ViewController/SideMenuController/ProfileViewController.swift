//
//  ProfileViewController.swift
//  illumine
//
//  Created by solulab on 3/26/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class ProfileViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UIDropDownDelegate
{
    @IBOutlet var btnProfile : UIButton!
    @IBOutlet var btnMedia : UIButton!
    @IBOutlet var lblDetail : UILabel!
    @IBOutlet var lblMedia : UILabel!
    var isFromProfile : Bool!
    @IBOutlet var viewEventDetail : UIView!
    @IBOutlet var viewHeader : UIView!
    @IBOutlet var viewAboutMe : UIView!
    @IBOutlet var dropDown : UIDropDown!
    @IBOutlet var viewAbsentDay : UIView!
    @IBOutlet var scrView : UIScrollView!
    @IBOutlet var collectionEventMedia : UICollectionView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        Utility.shared.dropShadow(view: viewAboutMe, color: blackColor, opacity: 0.3, radius: 3)
        Utility.shared.dropShadow(view: viewAbsentDay, color: blackColor, opacity: 0.3, radius: 3)

        Utility.shared.dropShadow(view: viewHeader, color: blackColor, opacity: 0.3, radius: 3)
        collectionEventMedia.clipsToBounds = false
        collectionEventMedia.layer.masksToBounds = false
        Utility.shared.dropShadow(view: collectionEventMedia, color: blackColor, opacity: 0.3, radius: 3)
        
        dropDown.placeholder = "Select Month"
        dropDown.font = UIFont.init(name: "OpenSens-Regular", size: 15)
        dropDown.delegate = self
        dropDown.tableHeight = 380
        dropDown.title.textAlignment = .left
        dropDown.tableY = dropDown.tableHeight + dropDown.frame.height + 10
        dropDown.options = ["January", "February", "March", "April", "May", "June", "July", "August","September", "October", "November", "December"]
//        Utility.shared.dropShadow(view: self.dropDown, color: UIColor.black, opacity: 0.3, radius: 0.3)

        if isFromProfile == true
        {
            btnProfilePress(UIButton())
        }
        else
        {
            btnMediaPress(UIButton())
        }
        
       
    }
    @IBAction func btnEditPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    func dropDown(_ dropDown: UIDropDown, didSelectOption option: AnyObject, atIndex index: Int) {
        _ = dropDown.resignFirstResponder()
        print("You just select: \(option) at index: \(index)")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionEventMedia.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoCell
        cell.imgView.image = #imageLiteral(resourceName: "activity_placeholder")
        return cell
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfilePress(_ radioButton: UIButton)
    {
        btnProfile.setImage(#imageLiteral(resourceName: "profile_selected"), for: .normal)
        btnMedia.setImage(#imageLiteral(resourceName: "media"), for: .normal)
        lblDetail.textColor = BlueColor
        lblMedia.textColor = UIColor.init(red: 161/255, green: 161/255, blue: 161/255, alpha: 1)
       
        scrView.isHidden = false
        collectionEventMedia.isHidden = true
    }
    
    @IBAction func btnMediaPress(_ radioButton: UIButton)
    {
        btnProfile.setImage(#imageLiteral(resourceName: "profile"), for: .normal)
        btnMedia.setImage(#imageLiteral(resourceName: "media_selected"), for: .normal)
        lblMedia.textColor = BlueColor
        lblDetail.textColor = UIColor.init(red: 161/255, green: 161/255, blue: 161/255, alpha: 1)
        
        scrView.isHidden = true
        collectionEventMedia.isHidden = false
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
