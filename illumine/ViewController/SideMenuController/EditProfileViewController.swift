//
//  EditProfileViewController.swift
//  illumine
//
//  Created by solulab on 4/4/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController
{

    @IBOutlet var btnFemale : UIButton!
    @IBOutlet var btnMale : UIButton!
    var isFemale : Bool!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Edit User"
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnFemalePress(sender:UIButton)
    {
        btnFemale.setImage(#imageLiteral(resourceName: "radio-fill"), for: .normal)
        btnMale.setImage(#imageLiteral(resourceName: "radio-empty"), for: .normal)
        isFemale = true
    }
    @IBAction func btnMalePress(sender:UIButton)
    {
        btnMale.setImage(#imageLiteral(resourceName: "radio-fill"), for: .normal)
        btnFemale.setImage(#imageLiteral(resourceName: "radio-empty"), for: .normal)
        isFemale = false
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
