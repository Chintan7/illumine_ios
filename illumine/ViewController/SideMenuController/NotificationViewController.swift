//
//  NotificationViewController.swift
//  illumine
//
//  Created by solulab on 4/4/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController,UIDropDownDelegate,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var dropDown : UIDropDown!
    @IBOutlet var tblObj : UITableView!
    @IBOutlet var viewContainer : UIView!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Notifications"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.tabBarController?.tabBar.isHidden = true
        tblObj.tableFooterView = UIView()
        
        dropDown.placeholder = "Select Month"
        dropDown.font = UIFont.init(name: "OpenSens-Regular", size: 16)
        dropDown.delegate = self
        dropDown.tableHeight = 400
        dropDown.title.textAlignment = .left
        dropDown.options = ["January", "February", "March", "April", "May", "June", "July", "August","September", "October", "November", "December"]
        
//     Utility.shared.dropShadow(view: self.viewContainer, color: UIColor.black, opacity: 0.3, radius: 0.3)
//     viewContainer.clipsToBounds = false
//     viewContainer.layer.masksToBounds = false
        
        viewContainer.backgroundColor = UIColor.clear
        viewContainer.layer.shadowColor = UIColor.black.cgColor
        viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewContainer.layer.shadowOpacity = 0.3
        viewContainer.layer.shadowRadius = 2
    }
    
    func dropDown(_ dropDown: UIDropDown, didSelectOption option: AnyObject, atIndex index: Int)
    {
        _ = dropDown.resignFirstResponder()
        print("You just select: \(option) at index: \(index)")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as! notificationCell
        cell.selectionStyle = .none
        return cell
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

class notificationCell: UITableViewCell
{
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var imgView : UIImageView!
}
