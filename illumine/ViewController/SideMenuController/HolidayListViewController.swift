//
//  HolidayListViewController.swift
//  illumine
//
//  Created by solulab on 4/3/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

class HolidayListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Holiday List"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
       
        self.tabBarController?.tabBar.isHidden = true
        tblObj.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as! holidayCell
        cell.selectionStyle = .none
        return cell
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
class holidayCell: UITableViewCell {
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var lblName : UILabel!
}
