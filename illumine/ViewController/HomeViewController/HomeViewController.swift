//
//  HomeViewController.swift
//  illumine
//
//  Created by solulab on 3/23/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import FirebaseDatabase
import MBProgressHUD
import SDWebImage

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SlideMenuDelegate,UIDropDownDelegate
{
    @IBOutlet var tblObj : UITableView!
    @IBOutlet var containerView : UIView!
    var SWRevalViewC : SWRevealViewController!
    @IBOutlet var tblHeight : NSLayoutConstraint!
    @IBOutlet var viewHeight : NSLayoutConstraint!
    @IBOutlet var dropDown : UIDropDown!
    @IBOutlet var imgLine : UIImageView!
    @IBOutlet var RecentView : UIView!
    var selectedDate : String!
    var dictTimeLine = NSDictionary()
    var arrTimeLine = Array<JsonDictionary>()
    var arrimgsUrl = [[String]]()

    @IBOutlet weak var datepicker: ScrollableDatepicker!
        {
        didSet {
            var dates = [Date]()
            for day in -365...0
            {
                dates.append(Date(timeIntervalSinceNow: Double(day * 86400)))
            }
            datepicker.dates = dates
            datepicker.selectedDate = Date()
            datepicker.delegate = self
            
            var configuration = Configuration()
            
            // selected date customization
            configuration.selectedDayStyle.backgroundColor = DarkBlueColor
            configuration.daySizeCalculation = .numberOfVisibleItems(5)
            
            datepicker.configuration = configuration
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        SWRevalViewC = self.revealViewController()
        SWRevalViewC.tapGestureRecognizer()
        
        self.navigationItem.title = "Primary Colors"

        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.menuButtonPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let button1: UIButton = UIButton.init(type:UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "notification"), for: UIControlState.normal)
        button1.addTarget(self, action: #selector(self.btnNotificationPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItem = barButton1
        
        self.showSelectedDate()
        self.datepicker.scrollToSelectedDate(animated: false)
        imgLine.backgroundColor = UIColor.init(patternImage: #imageLiteral(resourceName: "line"))

        containerView.backgroundColor = UIColor.clear
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowRadius = 2
        
        self.tblObj.layer.cornerRadius = 3
        self.tblObj.layer.masksToBounds = true
        
        dropDown.backgroundColor = UIColor.init(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)

        dropDown.placeholder = "Select Activity To Filter"
        dropDown.cornerRadius = 5
        dropDown.font = UIFont.init(name: "OpenSens-Regular", size: 16)
        dropDown.delegate = self
        dropDown.tableHeight = 300
        dropDown.title.textAlignment = .center
        dropDown.options = ["Photo", "Video", "Diary", "Kudos", "Milestone", "Assessment", "Nap", "Potty","Note", "Medicine", "Incident", "Observation"]

        Utility.shared.dropShadow(view: RecentView, color: UIColor.black, opacity: 0.3, radius: 5)
        RecentView.layer.shadowColor = UIColor.black.cgColor
        RecentView.layer.shadowOffset = CGSize(width: 0, height: 0)
        RecentView.layer.shadowOpacity = 0.5
        RecentView.layer.shadowRadius = 2
    }
    
    func dropDown(_ dropDown: UIDropDown, didSelectOption option: AnyObject, atIndex index: Int)
    {
        _ = dropDown.resignFirstResponder()
        print("You just select: \(option) at index: \(index)")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        
        getHomeList()
    }
    
    @IBAction func menuButtonPress(sender:UIButton)
    {
        self.view.endEditing(true)
        SWRevalViewC.revealToggle(sender)
        
        appDel.sideMenuviewObj.delegate = self
    }
    
    func getHomeList()
    {
//        first get studentId from user table
        appDel.showHUD()
        //get studentId from current user
        let userDetail = UserDefaults.standard.value(forKey: "userProfile") as! [String:Any]
        let studentId = "\(userDetail["studentId"]!)"
        print(studentId)
        
        getStudentId(id: studentId)
        // Go to student table using studentID and get id
    }
    
    //        studentId >students Table >get id
    func getStudentId(id:String!) {
        let currentUser = Database.database().reference().child("students")
        currentUser.child("-L7DRKE5YvAFt0c1nHz0").observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
                print("not found")
            } else {
                print(snapshot.value!)
                if let dict = snapshot.value as? [String:Any]
                {
                    let Id = "\(dict["id"]!)"
                    self.getTimelineId(id: Id)
                }
            }
        })
    }
    
//     timeline Table > find data with id >
    func getTimelineId(id:String!)
    {
        let currentUser = Database.database().reference().child("timeline")
        currentUser.child(id).observeSingleEvent(of: .value, with: { (snapshot) in
            if ( snapshot.value is NSNull ) {
                print("not found")
            }
            else
            {
                //get all date
                print(snapshot.value!)
                
                self.dictTimeLine = snapshot.value! as! NSDictionary
                self.getDataOnDateSelect()
            }
        })
    }
    
    // from date get activityId > Activity Table > get All Activity Data
    func getDataOnDateSelect()
    {
        self.arrTimeLine = Array()
        let arrKeys = dictTimeLine.allKeys as! [String]
        print(arrKeys)
        tblObj.isHidden = false
        if arrKeys.contains(self.selectedDate)
        {
            print("\(String(describing: dictTimeLine[self.selectedDate!]!))")
            let dictData = dictTimeLine[self.selectedDate!] as! NSDictionary
            let arr = dictData.allKeys as! [String]
            print(arr)
            for dict in arr
            {
                appDel.hideHUD()
                let tmpDict = dictData[dict] as! JsonDictionary
                let activityId = "\(tmpDict["activityId"]!)"
                print(activityId)
                
                let currentUser = Database.database().reference().child("activities")
                currentUser.child(activityId).observeSingleEvent(of: .value, with: { (snapshot) in
                    if ( snapshot.value is NSNull )
                    {
                        print("not found")
                    }
                    else
                    {
                        //get all date
                        let dict = snapshot.value! as! JsonDictionary

                        self.arrTimeLine.append(dict)
                        print(self.arrTimeLine)
                        if let arrImgs = dict["mediaPaths"] as? [String]
                        {
                            self.arrimgsUrl.append(arrImgs)
                        }
                        self.tblObj.reloadData()
                    }
                })
            }
        }
        else
        {
            appDel.hideHUD()
            self.tblObj.reloadData()
            tblObj.isHidden = true
        }
    }
    @IBAction func btnNotificationPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    func pushViewController(viewcontroller: UIViewController) {
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    func menuButtonpress()
    {
        SWRevalViewC.revealToggle(self.navigationItem.leftBarButtonItem)
    }
    fileprivate func showSelectedDate() {
        guard let selectedDate = datepicker.selectedDate else
        {
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM YYYY"
        print("\(formatter.string(from: selectedDate))")
        self.selectedDate = "\(formatter.string(from: selectedDate))"
        appDel.showHUD()
        getDataOnDateSelect()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let dict = arrTimeLine[indexPath.row] as JsonDictionary
        let name = "\(dict["name"]!)"
        if name == "Photo"
        {
            return 210.0
        }
        else
        {
            return 80.0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTimeLine.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as! ActivityCell
        cell.selectionStyle = .none
        cell.imgLine.backgroundColor = UIColor.init(patternImage: #imageLiteral(resourceName: "line"))
        let dict = arrTimeLine[indexPath.row] as JsonDictionary
        let name = "\(dict["name"]!)"
        cell.lblActivityName.text = name
        cell.lblActivityDetail.text = "\(dict["message"]!)"
        let dateDict = dict["date"] as! JsonDictionary
        if let arrrMedia = dict["mediaPaths"] as? [String]
        {
            let strUrl = "\(arrrMedia[0])"
            let url = URL(string: strUrl)
            cell.imgActivity.sd_setImage(with: url, placeholderImage:#imageLiteral(resourceName: "activity_placeholder"))
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date12 = dateFormatter.date(from: "\(dateDict["hours"]!):\(dateDict["minutes"]!)")!
        
        dateFormatter.dateFormat = "hh:mm a"
        let date22 = dateFormatter.string(from: date12)

        cell.lblActivityTime.text = "\(date22)"
        
        if name == "Photo"
        {
            cell.imgview.image = #imageLiteral(resourceName: "photo")
            cell.imgHeight.constant = 150
        }
        else if name == "Kudos"
        {
            cell.imgview.image = #imageLiteral(resourceName: "star")
            cell.imgHeight.constant = 0
        }
        else if name == "Diary"
        {
            cell.imgview.image = #imageLiteral(resourceName: "journal")
            cell.imgHeight.constant = 0
        }
        else if name == "Nap"
        {
            cell.imgview.image = #imageLiteral(resourceName: "nap")
            cell.imgHeight.constant = 0
        }
        else if name == "Milestone"
        {
            cell.imgview.image = #imageLiteral(resourceName: "achievement")
            cell.imgHeight.constant = 0
        }
        else if name == "Food"
        {
            cell.imgview.image = #imageLiteral(resourceName: "achievement")
            cell.imgHeight.constant = 0
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "ViewPhotosViewController") as! ViewPhotosViewController
            viewObj.arrImgUrl = arrimgsUrl[indexPath.row]
            self.pushViewController(viewcontroller: viewObj)
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
// MARK: - ScrollableDatepickerDelegate

extension HomeViewController: ScrollableDatepickerDelegate
{
    func datepicker(_ datepicker: ScrollableDatepicker, didSelectDate date: Date)
    {
        showSelectedDate()
    }
}
class ActivityCell : UITableViewCell
{
    @IBOutlet var imgLine : UIImageView!
    @IBOutlet var imgActivity : UIImageView!
    @IBOutlet var imgview : UIImageView!
    @IBOutlet var lblActivityName : UILabel!
    @IBOutlet var lblActivityDetail : UILabel!
    @IBOutlet var lblActivityTime : UILabel!
    @IBOutlet var imgHeight : NSLayoutConstraint!
}

