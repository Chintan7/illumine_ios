//
//  ViewPhotosViewController.swift
//  illumine
//
//  Created by solulab on 3/26/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

class ViewPhotosViewController: UIViewController,UIScrollViewDelegate
{
    var arrImgUrl = [String]()
        @IBOutlet var pageControll : UIPageControl!
        var currentIndex : Int!
        @IBOutlet var scrBanner : UIScrollView!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        pageControll.numberOfPages = arrImgUrl.count
        
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let pagesScrollViewSize = CGSize(width: UIScreen.main.bounds.width,height: scrBanner.frame.size.height)
        scrBanner.contentSize = CGSize(width: pagesScrollViewSize.width * CGFloat(arrImgUrl.count),height: pagesScrollViewSize.height)
        //        scrBanner.contentSize = CGSize.init(width: (scrBanner.frame.width) * 5, height: 130)
        var cnt : CGFloat = 0
        currentIndex = 0
        for i in 0...arrImgUrl.count - 1
        {
            cnt = CGFloat(i) * (UIScreen.main.bounds.width)
            let imgView = UIImageView(frame: CGRect.init(x: cnt, y: 0, width: UIScreen.main.bounds.width, height: scrBanner.contentSize.height))
            imgView.tag = i
            imgView.contentMode = .scaleAspectFit
            imgView.sd_setImage(with: URL.init(string: "\(arrImgUrl[i])"), placeholderImage: #imageLiteral(resourceName: "activity_placeholder"), options: [], completed: nil)
            scrBanner.addSubview(imgView)
        }
    }
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    @IBAction func changePage(sender : UIPageControl)
    {
        let x = CGFloat(pageControll.currentPage) * scrBanner.frame.size.width
        scrBanner.setContentOffset(CGPoint.init(x: x, y: 0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let pageNumber = round(scrBanner.contentOffset.x / scrBanner.frame.size.width)
        pageControll.currentPage = Int(pageNumber)
        currentIndex = pageControll.currentPage
    }

    @IBAction func btnSharePress(sender:UIButton)
    {
        // set up activity view controller
//        let textToShares = [img.sd_setImage(with: URL.init(string: "\(arrImgUrl[pageControll.currentPage]!)"), placeholderImage: #imageLiteral(resourceName: "activity_placeholder"))]
       
        let url = URL(string: "\(arrImgUrl[currentIndex])")
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
        
            let activityViewController = UIActivityViewController(activityItems: [image!], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnDownloadPress(sender:UIButton)
    {
        let url = URL(string: "\(arrImgUrl[currentIndex])")
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
extension ViewPhotosViewController :  UIImagePickerControllerDelegate
{
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "Saved!", message: "Image saved successfully", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true)
        }
    }
}
