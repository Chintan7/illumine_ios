//
//  CustomTabBarViewController.swift
//  AN Client
//
//  Created by Hetal Govani on 02/08/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit

protocol CustomTabBarControllerDelegate
{
    func customTabBarControllerDelegate_CenterButtonTapped(tabBarController:CustomTabBarViewController, button:UIButton, buttonState:Bool)
}

class CustomTabBarViewController: UITabBarController, UITabBarControllerDelegate
{
    var customTabBarControllerDelegate:CustomTabBarControllerDelegate?;
    var centerButton:UIButton!;
    var centerButtonTappedOnce:Bool = false;
    

    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews();
//        self.bringcenterButtonToFront();
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
     
        selectedMenuItem = 1
    
        self.delegate = self;
        self.tabBar.backgroundColor = UIColor.white
        
        self.tabBar.tintColor = BlueColor

        let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        let dashboardItem = self.tabBar.items![0] as UITabBarItem
        dashboardItem.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        
        let dashboardItemImage = UIImage(named:"Home_deselect")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let dashboardItemImageSelected = UIImage(named: "Home")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        dashboardItem.image = dashboardItemImage
        dashboardItem.selectedImage = dashboardItemImageSelected.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

        let nav1 = UINavigationController(rootViewController: dashboardVC)
        
        let view2Obj = self.storyboard?.instantiateViewController(withIdentifier: "EventViewController") as! EventViewController
        let view2Item = self.tabBar.items![1] as UITabBarItem
        view2Item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        let view2ItemImage = UIImage(named:"Event_deselect")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

        let view2ItemImageSelected = UIImage(named: "Event")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        view2Item.image = view2ItemImage

        view2Item.selectedImage = view2ItemImageSelected
        let nav2 = UINavigationController(rootViewController: view2Obj)
        
        let view3Obj = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        let view3Item = self.tabBar.items![2] as UITabBarItem
        view3Item.imageInsets = UIEdgeInsets(top: 9, left: 0, bottom: -9, right: 0)
        let view3ItemImage = UIImage(named:"Chat_deselect")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let view3ItemImageSelected = UIImage(named: "Chat")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        view3Item.image = view3ItemImage
        view3Item.selectedImage = view3ItemImageSelected
        let nav3 = UINavigationController(rootViewController: view3Obj)
        
        viewControllers = [nav1, nav2, nav3]

    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
   
   override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
   {
      
   }
    // MARK: - TabbarDelegate Methods
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)
    {
        switch viewController
        {
        case is HomeViewController:
         break
        case is EventViewController:
         break
        case is ChatViewController:
            break
        default: break
        }
    }
   @objc func gotoRootView()
   {
      
//      UIApplication.shared.keyWindow?.rootViewController?.navigationController?.popToRootViewController(animated: true)
   }
}

extension UIImage
{
    func imageWithColor(color: UIColor, size: CGSize) -> UIImage
    {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
