//
//  ChatDetailViewController.swift
//  illumine
//
//  Created by solulab on 4/5/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

class ChatDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet var tblObj : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        self.title = "Ms. Gertrude Mondanza"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MychatDetailCell
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "otherCell") as! OtherchatDetailCell
            return cell
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
class MychatDetailCell : UITableViewCell
{
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var lblTime : UILabel!
}
class OtherchatDetailCell : UITableViewCell
{
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var lblTime : UILabel!
}
