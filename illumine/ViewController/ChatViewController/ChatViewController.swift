//
//  ChatViewController.swift
//  illumine
//
//  Created by solulab on 3/23/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SlideMenuDelegate
{
    
    
    
    @IBOutlet var tblObj : UITableView!
    var SWRevalViewC : SWRevealViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Messages"
        tblObj.tableFooterView = UIView()
        SWRevalViewC = self.revealViewController()
        SWRevalViewC.tapGestureRecognizer()
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.menuButtonPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        
    }
    @IBAction func menuButtonPress(sender:UIButton)
    {
        self.view.endEditing(true)
        SWRevalViewC.revealToggle(sender)
        appDel.sideMenuviewObj.delegate = self
    }
    
    func menuButtonpress()
    {
        SWRevalViewC.revealToggle(self.navigationItem.leftBarButtonItem)
    }
    func pushViewController(viewcontroller: UIViewController) {
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblObj.dequeueReusableCell(withIdentifier: "cell") as! chatCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailViewController") as! ChatDetailViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
class chatCell: UITableViewCell {
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblName : UILabel!
}
