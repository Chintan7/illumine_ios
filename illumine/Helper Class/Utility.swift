//
//  UtilityClass.swift
//  illumine
//
//  Created by solulab on 3/26/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit

class Utility
{
    static let shared: Utility = Utility()

    func dropShadow(view:UIView,color: UIColor, opacity: Float, radius: CGFloat) {
        view.layer.masksToBounds = false

        view.layer.cornerRadius = radius
        view.layer.shadowOffset = CGSize.init(width: -1, height: 1)
        view.layer.shadowRadius = radius
        view.layer.shadowOpacity = opacity
        
    }
}
