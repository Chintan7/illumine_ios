//
//  CommonMethods.swift
//  Bevvi
//
//  Created by Hetal Govani on 26/12/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import CoreLocation

extension UITabBarController {
   func setBadges(badgeValues:[Int]){
      
      var labelExistsForIndex = [Bool]()
      
      for value in badgeValues {
         labelExistsForIndex.append(false)
      }
      
      for view in self.tabBar.subviews {
         if view.isKind(of: PGTabBadge.self) {
            let badgeView = view as! PGTabBadge
            let index = badgeView.tag
            
            if badgeValues[index]==0 {
               badgeView.removeFromSuperview()
            }
            
            labelExistsForIndex[index]=true
            badgeView.text = String(badgeValues[index])
            
         }
      }
      
      for i in 0 ..< labelExistsForIndex.count
      {
         if labelExistsForIndex[i] == false {
            if badgeValues[i] > 0 {
               addBadge(index: i, value: "\(badgeValues[i])", color:UIColor(red: 4/255, green: 110/255, blue: 188/255, alpha: 1), font: UIFont(name: "Helvetica-Light", size: 11)!)
            }
         }
      }
   }
   
   func addBadge(index:Int,value:String, color:UIColor, font:UIFont){
      
      let itemPosition = CGFloat(index+1)
      let itemWidth:CGFloat = tabBar.frame.width / CGFloat(tabBar.items!.count)
      
      let bgColor = color
      
      let xOffset:CGFloat = 12
      let yOffset:CGFloat = -9
      
      let badgeView = PGTabBadge()
//      badgeView.clipsToBounds=true
      badgeView.layer.masksToBounds=true

      badgeView.frame.size=CGSize.init(width: 20, height: 20)
      badgeView.center=CGPoint.init(x: (itemWidth * itemPosition)-(itemWidth/2)+xOffset, y: 20+yOffset)
      badgeView.layer.cornerRadius=badgeView.bounds.width/2
      badgeView.layer.borderColor = UIColor.white.cgColor
      badgeView.layer.borderWidth = 1
      badgeView.textColor=UIColor.white
      badgeView.textAlignment = .center
      badgeView.font = font
      badgeView.text = value
      badgeView.backgroundColor = bgColor
      badgeView.tag=index
      tabBar.addSubview(badgeView)
   }
   func removeBadge(index:Int)
   {
      for view in self.tabBar.subviews {
         
         if view.isKind(of: PGTabBadge.self) {
         
            let badgeView = view as! PGTabBadge
            if badgeView.tag == index
            {
               badgeView.removeFromSuperview()
            }
         }
      }
   }
}
class PGTabBadge: UILabel {
   
}
func estimatedHeightOfLabel(view:UIView,text: String) -> CGFloat
{
   let size = CGSize(width: view.frame.width - 16, height: 1000)
   
   let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
   
   let attributes = [NSAttributedStringKey.font: UIFont.init(name: "Avenir-Book", size: 15)]
   
   let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
   
   return rectangleHeight
}
