//
//  AppDelegate.swift
//  illumine
//
//  Created by solulab on 3/23/18.
//  Copyright © 2018 solulab. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Firebase
import FirebaseAuthUI
import IQKeyboardManagerSwift
import MBProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SWRevealViewControllerDelegate
{
    var window: UIWindow?
    var tabbarController : CustomTabBarViewController!
    var SWSideMenureval : SWRevealViewController!
    var sideMenuviewObj : MenuViewController!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      /*  if (UserDefaults.standard.object(forKey: "isLogin") == nil)
        {
            UserDefaults.standard.set(false, forKey: "isLogin")
            UserDefaults.standard.synchronize()
        }*/
        FirebaseApp.configure()

        IQKeyboardManager.sharedManager().enable = true

        let navigationBarAppearace = UINavigationBar.appearance()
        
        application.statusBarStyle = UIStatusBarStyle.lightContent
        navigationBarAppearace.tintColor = kBARBUTTONCOLOR
        navigationBarAppearace.barTintColor = BlueColor
        // change navigation item title color
        let navBackgroundImage:UIImage! = #imageLiteral(resourceName: "header")
        navigationBarAppearace.setBackgroundImage(navBackgroundImage?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
//        navigationBarAppearace.shadowImage = UIImage()

        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:kBARBUTTONCOLOR,NSAttributedStringKey.font:UIFont(name: "OpenSans-Semibold", size: 17)!]
//        let isLogin = UserDefaults.standard.bool(forKey: "isLogin")
        if Auth.auth().currentUser == nil
        {
            let loginObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginWithEmailOrPhoneViewController") as! LoginWithEmailOrPhoneViewController
            let navObj = UINavigationController.init(rootViewController: loginObj)
            self.window?.rootViewController = navObj
        }
        else
        {
            sideMenuviewObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
            tabbarController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
            SWSideMenureval = SWRevealViewController(rearViewController: sideMenuviewObj, frontViewController: tabbarController)
            
            SWSideMenureval.delegate = self
            self.window?.rootViewController = SWSideMenureval
        }
     /*   if isLogin == false
        {
            let loginObj = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginWithEmailOrPhoneViewController") as! LoginWithEmailOrPhoneViewController
            let navObj = UINavigationController.init(rootViewController: loginObj)
            self.window?.rootViewController = navObj
        }
        else
        {
            sideMenuviewObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
            tabbarController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomTabBarViewController") as! CustomTabBarViewController
            SWSideMenureval = SWRevealViewController(rearViewController: sideMenuviewObj, frontViewController: tabbarController)
            
            SWSideMenureval.delegate = self
            self.window?.rootViewController = SWSideMenureval
        }*/
        Fabric.with([Crashlytics.self])

        return true
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Pass device token to auth
        Auth.auth().setAPNSToken(deviceToken, type: .prod)
    }
    func showHUD()
    {
        hideHUD()
        MBProgressHUD.showAdded(to: (UIApplication.shared.keyWindow?.rootViewController?.view)!, animated: true)
    }
    
    func hideHUD()
    {
        MBProgressHUD.hide(for: (UIApplication.shared.keyWindow?.rootViewController?.view)!, animated: true)
    }
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

